<?php

namespace Drupal\block_marquee\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Provides 'Block Marquee'.
 * @Block(
 *  id = "block_marquee",
 *  admin_label = @Translation("Block Marquee"),
 * )
*/
class MarqueeBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $speed_options = [];
    $speed_options['1'] = $this->t('Very Slow');
    $speed_options['2'] = $this->t('Slow');
    $speed_options['3'] = $this->t('Medium');
    $speed_options['4'] = $this->t('Fast');
    $speed_options['5'] = $this->t('Very Fast');

    $scroll_directions = [];
    $scroll_directions['left'] = t('Left');
    $scroll_directions['right'] = t('Right');

    $scroll_behaviors = [];
    $scroll_behaviors['scroll'] = t('Scroll');
    $scroll_behaviors['slide'] = t('Slide');
    $scroll_behaviors['alternate'] = t('Alternate');

    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Scrolling Message'),
      '#description' => t('Enter your scrolling message here.  HTML tags are allowed.'),
      '#default_value' => isset($config['message']) ? $config['message'] : 'Hello This Is First Marquee Block',
    );
    $form['scroll_speed'] = array(
      '#type' => 'select',
      '#title' => $this->t('Scroll Speed'),
      '#options' => $speed_options,
      '#default_value' => isset($config['scroll_speed' ]) ? $config['scroll_speed'] : '3',
    );
    $form['scroll_direction'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Scroll Direction'),
      '#options' => $scroll_directions,
      '#default_value' => isset($config['scroll_direction']) ? $config['scroll_direction'] : 'left',
    );
    $form['scroll_behavior'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Scroll Behavior'),
      '#options' => $scroll_behaviors,
      '#default_value' => isset($config['scroll_behavior']) ? $config['scroll_behavior'] : 'scroll',
    );
     return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('message', $form_state->getValue(['message']));
    $this->setConfigurationValue('scroll_speed', $form_state->getValue(['scroll_speed']));
    $this->setConfigurationValue('scroll_direction', $form_state->getValue(['scroll_direction']));
    $this->setConfigurationValue('scroll_behavior', $form_state->getValue(['scroll_behavior']));
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return array(
      '#theme' => 'marquee-block',
      '#message' => isset($config['message']) ? $config['message'] : '',
      '#scroll_speed' => isset($config['scroll_speed']) ? $config['scroll_speed'] : '',
      '#scroll_direction' => isset($config['scroll_direction']) ? $config['scroll_direction'] : '',
      '#scroll_behavior' => isset($config['scroll_behavior']) ? $config['scroll_behavior'] : '',
      '#attached' => [
        'library' => [
          'block_marquee/marquee-js',
        ],
      ],
    );
  }
}
