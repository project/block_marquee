(function($){
  
  Drupal.behaviors.MarqueeBlock = {
    attach: function(context, settings) {
      
      //var blocks = $('marquee.marquee-block');
		var marquee = document.querySelector('.marquee');
		var marqueeLength = marquee.clientWidth;
		var marqueeTravelTime = Math.ceil( marqueeLength / 100 );
		marquee.style.animation = `scrollLeft ${marqueeTravelTime}s linear infinite`;
		marquee.addEventListener('mouseover', (e)=>{
		  marquee.style['animation-play-state'] = 'paused';
		})
		marquee.addEventListener('mouseout', (e)=>{
		  marquee.style['animation-play-state'] = 'running';
		})
      console.log(marquee);
    }
  };
  
})(jQuery);